<?php
require 'vendor/autoload.php';

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

$app = new \Slim\Slim();
//header('Access-Control-Allow-Origin: *');  
//header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
//header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
//header("Access-Control-Allow-Headers: X-Requested-With, X-authentication, X-client"); //Allow JSON data to be consumed

date_default_timezone_set('America/New_York');

use RedBean_Facade as R;

//R::setup('mysql:host=localhost;dbname=snore_app','root','');
R::setup('mysql:host=snorkid.db.8909276.hostedresource.com;dbname=snorkid','snorkid','SKver#1db');

//R::freeze(true);

//R::debug( TRUE );

$app->get('/hello/:name', function ($name) {
    echo "Hello, $name";
});

$app->options('/(:name+)', function() use ($app) {
	header('Access-Control-Allow-Origin: *');  
	header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
	//header("Access-Control-Allow-Headers: X-Requested-With, X-authentication, X-client"); //Allow JSON data to be consumed
});

$validateAccessToken= function($app) {

   	return function () use ($app) {

		$token = $app->request()->get("token");
		//$auth = R::find( 'authorization', ' auth_token = ? AND expiration > ? ', [$token, date("Y-m-d H:i:s") ]);
		$account_id = R::getCell( 'SELECT account_id FROM authorization WHERE auth_token = ? AND expiration > ? ', [$token, date("Y-m-d H:i:s") ] );

		if((int)$account_id > 0) {
			$app->user = R::load('account', $account_id);
		}
		else{
			
			$res = $app->response();
            $res['Content-Type'] = 'application/json';
            $res->status(403);
            $res->body(json_encode(array(
                'error' => 'Invalid or Expired authentication'
            )));
            $app->stop();
		}
    };
    
};

//create account
$app->post('/account', 'createAccount');

//get account
$app->get('/account', $validateAccessToken($app), 'getAccount');

//get account
$app->post('/update_account', $validateAccessToken($app), 'updateAccount');

//create profile
$app->post('/profile', $validateAccessToken($app),  'createProfile');


//list profiles
$app->get('/profiles', $validateAccessToken($app),  'listProfiles');

$app->post('/login', 'login');

//get question data
$app->get('/question/:profile_id/:quiz_id/:question_num', $validateAccessToken($app),  'getQuestion');

//generate new quiz_id
$app->get('/generate_quiz_id/:profile_id', $validateAccessToken($app),  'generateQuizId');

//list quizzes
$app->get('/list_quizzes/:profile_id', $validateAccessToken($app),  'listQuizzes');

//create or update question response
$app->post('/question_response', $validateAccessToken($app),  'createQuestionResponse');

//create addition info
$app->post('/additional_info', $validateAccessToken($app),  'createAdditionalInfo');

//get additional info response
$app->get('/additional_info/:profile_id/:quiz_id/:section', $validateAccessToken($app),  'getAdditionalInfo');

//get additional info response
$app->get('/prognosis/:profile_id/:quiz_id', $validateAccessToken($app),  'getPrognosis');

//send resent link
$app->post('/send_reset_link', 'sendResetLink');

//reset password form
$app->get('/reset_password', 'resetPasswordForm');

//reset password form
$app->post('/reset_password', 'resetPasswordSubmit');

//get summary of quiz results
$app->get('/results/:profile_id/:quiz_id/', $validateAccessToken($app), 'results');

//email summary of quiz results
$app->get('/email_results/:profile_id/:quiz_id/', $validateAccessToken($app), 'emailResults');

//quizReport
$app->get('/quiz_report', 'quizReport');

//quizReportCsv
$app->get('/quiz_report_csv', 'quizReportCsv');

function encodePassword($password){
	return md5($password."SALTYDOG");
}

function emailResults($profile_id, $quiz_id){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;

	include("emailResults.php");

	echo $message;

	if(mail($to, "Quiz Results", $message, $headers)){
		//echo "MAILED";
		return true;
	}
}

function get_quiz_result($profile_id, $quiz_id){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;
	$profile = R::findOne('profile', 'account_id = ? AND id = ?', [$account->id, $profile_id]);
	$question_query = "SELECT question.*, category.id as category_id FROM question 
	LEFT JOIN questionresponse ON questionresponse.question_id = question.id 
	LEFT JOIN category ON category.id = question.category_id 
	WHERE questionresponse.response = 'yes' 
	AND questionresponse.profile_id = :profile_id  
	AND questionresponse.quiz_id = :quiz_id 
	ORDER BY category.id, question.sequence";
	$rows = R::getAll($question_query, array('profile_id' => $profile_id, 'quiz_id' => $quiz_id) );
	$questions = R::convertToBeans('question',$rows);

	$responses_query  = "SELECT question.response_phrase , questionresponse.response
	FROM question
	LEFT JOIN questionresponse ON questionresponse.question_id = question.id
	LEFT JOIN category ON category.id = question.category_id 
	WHERE questionresponse.profile_id = :profile_id 
	AND questionresponse.quiz_id = :quiz_id 
	ORDER BY category.id, question.sequence";
	$all_responses = R::getAll($responses_query, array('profile_id' => $profile_id, 'quiz_id' => $quiz_id) );


	$yes_count =  R::count( 'questionresponse', 'profile_id = ? AND quiz_id = ? AND response = ?', [$profile_id, $quiz_id, 'yes']);

	$profile = R::findOne('profile', 'id = ?', [$profile_id]);
	$profile_name = $profile->name;

	if(strlen($profile->last_name) > 0){
		$profile_name .= " ".$profile->last_name;
	}

	$quiz = R::findOne('quiz', 'id = ?', [$quiz_id]);
	$quiz_datetime = date( "m/d/Y", strtotime($quiz->date_time));

	$prognosis = "negative";
	if($yes_count >= 4){
		$prognosis = "positive";
	}

	$output = array("prognosis" => $prognosis);
	$output["patient_number"] = $profile_id;
	$output["parent_name"] = $account->first_name . " " . $account->last_name ;
	$output["all_responses"] = $all_responses;

	$output["date_time"] = $quiz_datetime;
	$output["profile_name"] = $profile_name;
	$output["profile_dob"] = date("m/d/Y", strtotime($profile->date_of_birth));

	$output["response_categories"] = array();

	$category_responses = array();

	$gender = $profile->gender;

	$output["gender"] = $gender;

	$additionalinfo_rows = R::getAll( 'SELECT info_key, info_value FROM additionalinfo WHERE quiz_id = :quiz_id AND profile_id = :profile_id ', 
		array('profile_id' => $profile_id, 'quiz_id' => $quiz_id) );

	

	$additional_info = array();
	foreach ($additionalinfo_rows as $info_row) {
		$additional_info[$info_row['info_key']] = $info_row['info_value'];
	}
	
	$height = R::getCell( 'SELECT info_value FROM additionalinfo WHERE quiz_id = ? AND profile_id = ? AND info_key = ? ', 
		[ $quiz_id, $profile_id, 'height' ] );

	$weight = R::getCell( 'SELECT info_value FROM additionalinfo WHERE quiz_id = ? AND profile_id = ? AND info_key = ? ', 
		[ $quiz_id, $profile_id, 'weight' ] );
	list($height_feet, $height_inches) = explode(":", $height);

	$neck_size = R::getCell( 'SELECT info_value FROM additionalinfo WHERE quiz_id = ? AND profile_id = ? AND info_key = ? ', 
		[ $quiz_id, $profile_id, 'neck_size' ] );

	$neck_size = str_replace("dont_know", "Unknown", $neck_size);

	foreach($questions as $question){
		$he_she_rep_txt = ($gender == 'male') ? "He" : "She";
		$his_her_rep_txt = ($gender == 'male') ? "his" : "her";
		$response_phrase = $question->response_phrase;
		$response_phrase = str_replace("he/she", $he_she_rep_txt, $response_phrase);
		$response_phrase = str_replace("his/her", $his_her_rep_txt, $response_phrase);

		$category_responses[$question->category_id][] = array('id' => $question->id, 'question_text' => $response_phrase );
	}

	$categories = R::findAll( 'category' , ' ORDER BY id ' );

	foreach ($categories as $category) {
		if(count($category_responses[$category->id]) > 0){
			$output["response_categories"][] = array("category_image" => $category->image, "category_name" => $category->name,
				"responses" => $category_responses[$category->id]
			);
		}		
	}

	$additional_info['height'] = "$height_feet ft. $height_inches in.";
	$additional_info['weight'] = "$weight lbs.";
	$additional_info['neck_size'] = "$neck_size";

	$output["additional_info"] = $additional_info;

	$additional_info_resp = array(
		array('question_text' => "Height: $height_feet ft. $height_inches in."),
		array('question_text' => "Weight: $weight lbs."),
		array('question_text' => "Neck Size: $neck_size"),
	);

	$output["response_categories"][] = array("category_image" => "add-info.png", "category_name" => "Additional Info",
				"responses" => $additional_info_resp
			);

	$applies_to_child = R::getCell( 'SELECT info_value FROM additionalinfo WHERE quiz_id = ? AND profile_id = ? AND info_key = ? ', 
		[ $quiz_id, $profile_id, 'applies_to_child' ] );

	$applies_to_child = objectToArray(unserialize($applies_to_child));

	$cond_mapping = array(
		'diff_conc' => 'Has difficulty concentrating.',
		'trouble_ss' => 'Has trouble sitting still.',
		'probs_at_school' => 'Has behavior problems at school.',
		'poor_perf' => 'Has poor performance in school.',
		'tonsils_taken' => 'Had tonsils taken out.',
	);

	$child_conditions = array();
	foreach($applies_to_child as $key => $val){
		$child_conditions[] = $cond_mapping[$key];		
	}

	$all_conditions = array();
	foreach($cond_mapping as $cond_key => $cond) {
		if(isset($applies_to_child[$cond_key])){
			$all_conditions[$cond_mapping[$cond_key]] = "yes";
		}
		else{
			$all_conditions[$cond_mapping[$cond_key]] =  "no";
		}
	}
	$output["all_conditions"] = $all_conditions;

	$output["child_conditions"] = $child_conditions;

	return $output;	
}

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

function results($profile_id, $quiz_id){
	$output = get_quiz_result($profile_id, $quiz_id);
	echo json_encode($output);
}

function resetPasswordSubmit(){
	$success = false;
	if( !empty($_POST['password']) && ($_POST['password'] == $_POST['password_confirm']) ){
		if(isset($_POST['token'])){
			$account = R::findOne('account', 'reset_token = ?', [$_POST['token']] );
			if($account){
				$account->reset_token = null;
				$account->password = encodePassword($_POST['password']);
				R::store($account);
				$success = true;
			}
			
		}
	}
	if($success){
		echo "successfully reset password!";
	}
	else{
		echo "failed to reset password";
	}
}

function quizReportData(){
	$sql = "SELECT account.first_name, account.last_name, profile.id as patient_number, 
profile.name as child_first_name, profile.last_name as child_last_name, profile.gender, profile.date_of_birth,  
quiz.date_time as test_date,
additionalinfo.*,
questionresponse.* 
FROM `account` 
LEFT JOIN profile ON profile.account_id = account.id
LEFT JOIN quiz ON quiz.profile_id = profile.id

LEFT JOIN (

SELECT quiz_id, 
MAX(IF(info_key = 'height', info_value, NULL)) AS height,
MAX(IF(info_key = 'weight', info_value, NULL)) AS weight,
MAX(IF(info_key = 'neck_size', info_value, NULL)) AS neck_size,
MAX(IF(info_key = 'doc_name', info_value, NULL)) AS doc_name, 
MAX(IF(info_key = 'doc_address', info_value, NULL)) AS doc_address,
MAX(IF(info_key = 'doc_city', info_value, NULL)) AS doc_city, 
MAX(IF(info_key = 'doc_state', info_value, NULL)) AS doc_state, 
MAX(IF(info_key = 'applies_to_child', info_value, NULL)) AS applies_to_child  
FROM additionalinfo
GROUP BY quiz_id

) additionalinfo ON additionalinfo.quiz_id = quiz.id

LEFT JOIN (

SELECT quiz_id, 
MAX(IF(question_id = 1, response, NULL)) AS snores_everynight,
MAX(IF(question_id = 2, response, NULL)) AS snores_morethanhalf_night,
MAX(IF(question_id = 3, response, NULL)) AS snores_loudly,
MAX(IF(question_id = 4, response, NULL)) AS stops_breathing_while_sleeping,
MAX(IF(question_id = 5, response, NULL)) AS breathes_heavily_during_sleep,
MAX(IF(question_id = 6, response, NULL)) AS cries_while_asleep,
MAX(IF(question_id = 7, response, NULL)) AS rocks_head,
MAX(IF(question_id = 8, response, NULL)) AS wakes_up_tired,
MAX(IF(question_id = 9, response, NULL)) AS wakes_up_dry_mouth,
MAX(IF(question_id = 10, response, NULL)) AS sleeps_late_weekends,
MAX(IF(question_id = 11, response, NULL)) AS falls_asleep_unexpected,
MAX(IF(question_id = 12, response, NULL)) AS breathes_through_mouth,
MAX(IF(question_id = 13, response, NULL)) AS daytime_sleepiness,
MAX(IF(question_id = 14, response, NULL)) AS growth_rate_stop 
FROM questionresponse
GROUP BY quiz_id

) questionresponse ON questionresponse.quiz_id = quiz.id

where profile.name IS NOT NULL AND additionalinfo.quiz_id  IS NOT NULL 
ORDER BY quiz.date_time DESC LIMIT 1000 
";

$recs = R::getAll($sql);

return $recs;
}

function quizReport(){
	$recs = quizReportData();
	require_once("quizReport.php");
}

function quizReportCsv(){
	$recs = quizReportData();
	$filename = "quiz_report".strtotime("now").".csv";
	header( 'Content-Type: text/csv' );
    header( 'Content-Disposition: attachment;filename='.$filename);
	require_once("quizReportCsv.php");
}

function resetPasswordForm(){
	?>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>	
</head>
<body>
	<div class="container">

      <form class="form-signin" role="form" method="post" action="/reset_password">
        <h2 class="form-signin-heading">Reset Password</h2>
        <input type="hidden" class="form-control" name="token" value="<?php echo $_GET['token']?>">
        <input type="password" class="form-control" name="password" placeholder="Password" required>
        <input type="password" class="form-control" name="password_confirm" placeholder="Confirm Password " required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Reset</button>
      </form>

    </div> <!-- /container -->
</body>
</html>
	<?php
}

$app->get('/token_verify/:token', function($token){
	$auth = R::find( 'authorization', ' auth_token = ? AND expiration > ? ', [$token, date("Y-m-d H:i:s") ]);
	if($auth){
		$resp = array("status" => "valid_token");
		echo json_encode($resp);
	}
	else{
		$resp = array("error" => "invalid_token");
		echo json_encode($resp);
	}
});

$app->run();

function sendResetLink(){
	$data = file_get_contents("php://input");
	$data = json_decode($data);
	$account = R::findOne('account', 'email = ?', [$data->username]);

	$success = false;
	if($account){
		$reset_token = uniqid('P').uniqid('S');
		$account->reset_token = $reset_token;
		R::store($account);

		$reset_link  = "http://".$_SERVER['HTTP_HOST']."/reset_password?token={$reset_token}";

		$message = "Your password reset request has been received. Please clicking the 
		following link to complete your request. \n {$reset_link}";
		if(mail($account->email, "Snorkid Password reset", $message)){
			$success = true;
		}

		
	}

	if($success){
		$resp = array("status" => "success");
		echo json_encode($resp);
	}
	else{
		$resp = array("error" => "failed to reset password");
		echo json_encode($resp);
	}
	
}

function getPrognosis($profile_id, $quiz_id){
	$yes_count =  R::count( 'questionresponse', 'profile_id = ? AND quiz_id = ? AND response = ?', [$profile_id, $quiz_id, 'yes']);

	$prognosis = "negative";
	if($yes_count >= 4){
		$prognosis = "positive";
	}

	$resp = array("prognosis" => $prognosis);
	echo json_encode($resp);
}

function getAdditionalInfo($profile_id, $quiz_id, $section){
	$app = \Slim\Slim::getInstance();
	
	if($section == "section1"){
		$additionalinfos  = R::find( 'additionalinfo', 'profile_id = ? AND quiz_id = ? AND info_key IN ("height", "weight", "neck_size") ', 
			[$profile_id, $quiz_id]);
		$outputs = array();
		foreach($additionalinfos as $ainfo){
			if($ainfo->info_key == "height"){
				list($feet, $inches) = explode(":", $ainfo->info_value);
				$outputs[$ainfo->info_key] = array("feet" => $feet, "inches" => $inches);
			}
			else{
				$outputs[$ainfo->info_key] = $ainfo->info_value;	
			}
			
		}
		echo json_encode($outputs);
	}
	if($section == "section2"){
		$ainfo  = R::findOne( 'additionalinfo', 'profile_id = ? and quiz_id = ? AND info_key IN ("applies_to_child") ', 
			[$profile_id, $quiz_id]);
		$applies = unserialize($ainfo->info_value);
		echo json_encode($applies);
	}
	if($section == "section3"){
		$additionalinfos  = R::find( 'additionalinfo', 'profile_id = ? and quiz_id = ? AND info_key IN ("doc_name", "doc_address", "doc_city", "doc_state") ', 
			[$profile_id, $quiz_id]);
		$outputs = array();
		foreach($additionalinfos as $ainfo){
			$outputs[$ainfo->info_key] = $ainfo->info_value;
		}
		echo json_encode($outputs);
	}
}

function createAdditionalInfo(){
	$app = \Slim\Slim::getInstance();
	$data = file_get_contents("php://input");
	$data = json_decode($data);

	if($data->section == "section1"){
		foreach(array("height", "weight", "neck_size") as $key){
			$additionalinfo = R::dispense('additionalinfo');
			$additionalinfo = R::findOne('additionalinfo', 'profile_id = ? AND info_key = ? AND quiz_id = ?', 
				[$data->profile_id, $key, $data->quiz_id] );
			if(!$additionalinfo){
				$additionalinfo = R::dispense('additionalinfo');
			}	
			$additionalinfo->profile_id = $data->profile_id;
			$additionalinfo->quiz_id = $data->quiz_id;
			$additionalinfo->info_key = $key;
			$additionalinfo->info_value = $data->$key;

			$success = true;
			try{
				R::store($additionalinfo);				
			} catch (Exception $e) {
				$success = false;				
			}
				
		}

		if($success){
			$resp = array("status" => "success", "redirect" => "additional_info_2.html");
			echo json_encode($resp);
		}
		else{
			$resp = array("error" => "failed to create info");
				echo json_encode($resp);
		}
	}

	if($data->section == "section2"){
		$key = "applies_to_child";
		$additionalinfo = R::findOne('additionalinfo', 'profile_id = ? AND info_key = ? AND quiz_id = ?', 
			[$data->profile_id, $key, $data->quiz_id] );
		if(!$additionalinfo){
			$additionalinfo = R::dispense('additionalinfo');
		}
		$additionalinfo->profile_id = $data->profile_id;
		$additionalinfo->quiz_id = $data->quiz_id;
		$additionalinfo->info_key = $key;
		$additionalinfo->info_value = serialize($data->applies_to_child);
		
		$success = true;
		try{
			R::store($additionalinfo);				
		} catch (Exception $e) {
			$success = false;				
		}

		if($success){
			$resp = array("status" => "success", "redirect" => "additional_info_3.html");
			echo json_encode($resp);
		}
		else{
			$resp = array("error" => "failed to create info");
				echo json_encode($resp);
		}
	}

	if($data->section == "section3"){
		foreach(array("doc_name", "doc_address", "doc_city", "doc_state") as $key){
			$additionalinfo = R::findOne('additionalinfo', 'profile_id = ? AND info_key = ?AND quiz_id = ?', 
				[$data->profile_id, $key, $data->quiz_id] );
			if(!$additionalinfo){
				$additionalinfo = R::dispense('additionalinfo');
			}	
			$additionalinfo->profile_id = $data->profile_id;
			$additionalinfo->quiz_id = $data->quiz_id;
			$additionalinfo->info_key = $key;
			$additionalinfo->info_value = $data->$key;

			$quiz = R::findOne('quiz', 'id = ?', [$data->quiz_id]);
			$quiz->date_time = date("Y-m-d H:i:s");

			$success = true;
			try{
				R::store($additionalinfo);				
			} catch (Exception $e) {
				$success = $success & false;				
			}
		}
		if($success){
			$resp = array("status" => "success", "redirect" => "prognosis.html");
			echo json_encode($resp);	
		}
		
	}
}


function createQuestionResponse(){
	$app = \Slim\Slim::getInstance();
	$data = file_get_contents("php://input");
	$data = json_decode($data);

	//fetch existing response or dispense a new one
	$questionresponse = R::findOne('questionresponse', 'question_id = ? AND profile_id = ? AND quiz_id = ?', 
		[$data->question_id, $data->profile_id, $data->quiz_id]);
	if(!$questionresponse){
		$questionresponse = R::dispense('questionresponse');	
		$questionresponse->quiz_id = $data->quiz_id;
		$questionresponse->profile_id = $data->profile_id;
		$questionresponse->question_id = $data->question_id;
	}
	$questionresponse->response = $data->response;

	try{
		R::store($questionresponse);
		$next_sequence = R::getCell( 'SELECT sequence FROM question 
		WHERE sequence > (SELECT sequence FROM question WHERE id = ?)
		ORDER BY sequence
		LIMIT 1', [$data->question_id] );
		$resp = array("status" => "success", "next_sequence" => $next_sequence);
		echo json_encode($resp);
	} catch (Exception $e) {
		$resp = array("error" => "failed to create response", "message" => $e->getMessage());
		echo json_encode($resp);
	}
	
}

function generateQuizId($profile_id){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;

	$quiz = R::dispense('quiz');
	$profile = R::findOne('profile', 'account_id = ? AND id = ?', [$account->id, $profile_id]);
	$quiz->profile_id = $profile->id;
	//$quiz->date_time = date("Y-m-d H:i:s");
	try{
		$id = R::store($quiz);
		$resp = array("status" => "success", "quiz_id" => $id);
		echo json_encode($resp);
	} catch( Exception $e){
 		$resp = array("error" => "failed to create quiz");
		echo json_encode($resp);
	}

}

function listQuizzes($profile_id){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;
	$profile = R::findOne('profile', 'account_id = ? AND id = ?', [$account->id, $profile_id]);
	$quizzes = R::find('quiz', 'profile_id = ? ', [$profile->id]);
	foreach($quizzes as $quiz){
		$output[] = array('id' => $quiz->id, 'profile_id' => $quiz->profile_id, 'date' => $quiz->date_time);
	}
	echo json_encode($output);
}

function getQuestion($profile_id, $quiz_id, $question_num){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;
	$question = R::findOne('question', 'sequence = ? ', [$question_num]);
	$category = $question->category;

	$response = "";
	$questionresponse = R::findOne('questionresponse', 'question_id = ? AND profile_id = ? AND quiz_id = ?', 
		[$question->id, $profile_id, $quiz_id]);
	if($questionresponse){
		$response = $questionresponse->response;
	}

	$output = array('id' => $question->id,'question_text' => $question->question_text, 'sequence' => $question->sequence, 
		'category_name' => $category->name, 'category_image' => $category->image, 'response' => $response );
	echo json_encode($output); 
}

function listProfiles(){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;
	$profiles = R::find('profile', 'account_id = ? ', [$account->id]);
	$output = array();
	foreach($profiles as $profile){
		$output[] = array('id' => $profile->id, 'name' => $profile->name, 'date_of_birth' => $profile->date_of_birth, 'gender' => $profile->gender );
	}
	echo json_encode($output);
}

function createProfile(){
	$app = \Slim\Slim::getInstance();
	#echo $app->request()->get("token")."\n";
	#echo "running createProfile";
	$data = file_get_contents("php://input");
	$data = json_decode($data);

	$profile = R::dispense('profile');
	$profile->name = $data->child_name;
	$profile->last_name = $data->child_last_name;
	$profile->date_of_birth = date("Y-m-d", strtotime($data->dob));
	$profile->gender = $data->gender;
	
	$account = $app->user;
	$profile->account_id = $account->id;

	try{
		$id = R::store($profile);
		$resp = array("status" => "success", "profile_id" => $id);
		echo json_encode($resp);
	} catch (Exception $e) {
		$resp = array("error" => "failed to create profile");
		echo json_encode($resp);
	}

}

function createAccount(){
	$data = file_get_contents("php://input");
	$data = json_decode($data, true);
	//$data = $_POST;

	$account = R::dispense('account');
	$account->first_name = $data['first_name'];
	$account->last_name = $data['last_name'];
	$account->email = $data['email'];
	$account->password = encodePassword($data['password']);
	$account->created_at = date("Y-m-d H:i:s");

	$count = R::getCell( 'SELECT count(*) count FROM account WHERE email = ? ', [ $account->email ] );

	if($count > 0){
		$resp = array("error" => "email account already exists");
		echo json_encode($resp);
	}
	else{
		try{
			$id = R::store($account);  
			if($id > 0){
				$resp = array("status" => "success");
				echo json_encode($resp);
			}

		} catch (Exception $e) {
			$resp = array("error" => "failed to create account");
			echo json_encode($resp);
		}
	}	
	
}

function getAccount(){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;

	if($account){
		$data['first_name'] = $account->first_name;
		$data['last_name'] = $account->last_name;
		$data['email'] = $account->email;
		$resp = array("status" => "success", "data" => $data);
		echo json_encode($resp);
	}
	else{
		$resp = array("error" => "failed to fetch account");
		echo json_encode($resp);
	}
	
}

function updateAccount(){
	$app = \Slim\Slim::getInstance();
	$account = $app->user;

	$data = file_get_contents("php://input");
	$data = json_decode($data, true);

	if($account){
		$count = R::getCell( 'SELECT count(*) count FROM account WHERE email = ? AND id <> ? ', [ $data['email'], $account->id ] );

		if($count > 0 ){
			$resp = array("error" => "supplied email already exists");
			echo json_encode($resp);
			return true;
		}

		$account->first_name = $data['first_name'];
		$account->last_name = $data['last_name'];
		$account->email = $data['email'];

		if($data['password'] != $data['password_confirm']){
			$resp = array("error" => "failed to update account");
			echo json_encode($resp);
			return true;
		}
		elseif (strlen($data['password']) > 0) {
			$password = encodePassword($data['password']);
			$account->password = $password;
		}

		if(R::store($account)){
			$resp = array("status" => "success");
			echo json_encode($resp);
		}
	}
	else{
		$resp = array("error" => "failed to update account");
		echo json_encode($resp);
	}
}

function login(){
	#echo "<br>input<br>\n";
	#print_r(file_get_contents("php://input"));
	$data = file_get_contents("php://input");
	$data = json_decode($data);
	//print_r($data);

	$authorization = R::dispense('authorization');
	$password = encodePassword($data->password);

	$id = R::getCell( 'SELECT id FROM account WHERE email = ? AND password = ? ', [ $data->email, $password ] );

	if($id > 0){
		$authorization->account_id = $id;
		$authorization->auth_token = md5(uniqid(mt_rand(), true));
		$authorization->expiration = date("Y-m-d H:i:s", strtotime("+1 day"));
		R::store($authorization);
		$resp = array("token" => $authorization->auth_token);
		echo json_encode($resp);
	}
	else{
		$resp = array("error" => "bad login. try again.");
		echo json_encode($resp);
	}
}