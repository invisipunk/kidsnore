<?php

	$fp = fopen('php://output', 'w');

	$data = array();

	$header = array(
	        "PARENT LAST",
	        "PARENT FIRST",
	        "CHILD LAST",
	        "CHILD FIRST", 
	        "PATIENT",    
	        "GENDER",
	        "HEIGHT",
	        "WEIGHT",
	        "NECK SIZE",
	        "DOB",
	        "DOCTOR NAME",
	        "DOCTOR ADDRESS",
	        "DOCTOR CITY",
	        "DOCTOR STATE",
	        "TEST DATE",
	        "SNORES EVERY NIGHT?",
	        "SNORES MORE THAN HALF THE NIGHT?",
	        "SNORES LOUDLY?",
	        "STOPS BREATHING DURING SLEEP?",
	        "BREATHES HEAVILY/ LOUDLY DURING SLEEP?",
	        "CRIES OUT/SCREAMS WHILE ASLEEP?",
	        "BANGS HEAD/ROCKS BACK &amp; FORTH WHILE SLEEPING?",
	        "WAKES IN THE MORNING FEELING TIRED?",
	        "WAKES W/ A DRY MOUTH?",
	        "SLEEPS LATE ON WKNDS?",
	        "FALLS ASLEEP AT UNEXPECTED TIMES DURING THE DAY",
	        "FREQUENTLY BREATHES TH/ MOUTH WHILE AWAKE",
	        "EXPERIENCES/COMPLAINS ABOUT DAYTIME SLEEPINESS",
	        "NORMAL GROWTH RATE HAS STOPPED ON OCCASION",
	        
	        "HAS DIFFICULTY CONCENTRATING",
	        "HAS TROUBLE SITTING STILL",
	        "HAS BEHAVIOR PROBLEMS AT SCHOOL",
	        "HAS POOR PERFORMANCE IN SCHOOL",
	        "HAD TONSILS REMOVED",
	        "TOTAL YES",
	        "LIKELIHOOD OF OSA");

	$data[] = $header;

	
	foreach($recs as $record){	
		$applies_to_child = unserialize($record['applies_to_child']); 
		$applies_to_child = objectToArray($applies_to_child);
	
		$csv_rec = array();
		
		$csv_rec[] = $record['last_name']; 
		$csv_rec[] = $record['first_name']; 		
		$csv_rec[] = $record['child_last_name']; 
		$csv_rec[] = $record['child_first_name']; 
		$csv_rec[] = $record['patient_number']; 
		$csv_rec[] = $record['gender']; 
		
		list($height_feet, $height_inches) = explode(":", $record['height']);
		$record['height'] = $height_feet."'";
		if((int)$height_inches > 0){
			$record['height'] .= ' '.$height_inches.'"';
		}

		$csv_rec[] = $record['height']; 
		$csv_rec[] = $record['weight']; 
		$csv_rec[] = $record['neck_size']; 
		$csv_rec[] = $record['date_of_birth']; 
		$csv_rec[] = $record['doc_name']; 
		$csv_rec[] = $record['doc_address']; 
		$csv_rec[] = $record['doc_city']; 
		$csv_rec[] = $record['doc_state']; 
		$csv_rec[] = $record['test_date']; 
		$csv_rec[] = $record['snores_everynight']; 
		$csv_rec[] = $record['snores_morethanhalf_night']; 
		$csv_rec[] = $record['snores_loudly']; 
		$csv_rec[] = $record['stops_breathing_while_sleeping']; 
		$csv_rec[] = $record['breathes_heavily_during_sleep']; 
		$csv_rec[] = $record['cries_while_asleep']; 
		$csv_rec[] = $record['rocks_head']; 
		$csv_rec[] = $record['wakes_up_tired']; 
		$csv_rec[] = $record['wakes_up_dry_mouth']; 
		$csv_rec[] = $record['sleeps_late_weekends']; 
		$csv_rec[] = $record['falls_asleep_unexpected']; 
		$csv_rec[] = $record['breathes_through_mouth']; 
		$csv_rec[] = $record['daytime_sleepiness']; 
		$csv_rec[] = $record['growth_rate_stop']; 

$bool_fields = array("snores_everynight",
"snores_morethanhalf_night",
"snores_loudly",
"stops_breathing_while_sleeping",
"breathes_heavily_during_sleep",
"cries_while_asleep",
"rocks_head",
"wakes_up_tired",
"wakes_up_dry_mouth",
"sleeps_late_weekends",
"falls_asleep_unexpected",
"breathes_through_mouth",
"daytime_sleepiness",
"growth_rate_stop");


		$yes_count = 0;
		foreach($bool_fields as $fkey){
			if($record[$fkey] == "yes"){
				$yes_count++;
			}
		}
		
		
		
		$csv_rec[] = isset($applies_to_child['diff_conc']) ? 'yes' : 'no' ;
		$csv_rec[] = isset($applies_to_child['trouble_ss']) ? 'yes' : 'no' ;
		$csv_rec[] = isset($applies_to_child['probs_at_school']) ? 'yes' : 'no' ;
		$csv_rec[] = isset($applies_to_child['poor_perf']) ? 'yes' : 'no' ;
		$csv_rec[] = isset($applies_to_child['tonsils_taken']) ? 'yes' : 'no' ;

		$csv_rec[] = $yes_count;

		$csv_rec[] = ($yes_count >= 4) ? "POSITIVE" : "NEGATIVE";
		
		$data[] = $csv_rec;
	}
	
	foreach($data as $data_row){
		fputcsv($fp, $data_row);
	}
	fclose($fp);