<?php
use RedBean_Facade as R;
?>
<div><a href="/quiz_report_csv">csv</a></div>
<table border="1" cellpadding="2">

            <thead>
            <tr>
            <th>PARENT LAST</th>
            <th>PARENT FIRST</th>           
            <th>CHILD LAST</th>
            <th>CHILD FIRST</th>
            <th>PATIENT</th>
            <th>GENDER</th>
            <th>HEIGHT</th>
            <th>WEIGHT</th>
            <th>NECK SIZE</th>
            <th>DOB</th>
            <th>DOCTOR<br />NAME</th>
            <th>DOCTOR<br />ADDRESS</th>
            <th>DOCTOR<br />CITY</th>
            <th>DOCTOR<br />STATE</th>
            <th>TEST<br />DATE</th>
            <th>SNORES EVERY<br />NIGHT?</th>
            <th>SNORES MORE THAN<br />HALF THE NIGHT?</th>
            <th>SNORES<br />LOUDLY?</th>
            <th>STOPS BREATHING<br />DURING SLEEP?</th>
            <th>BREATHES HEAVILY/<br />LOUDLY DURING SLEEP?</th>
            <th>CRIES OUT/SCREAMS<br />WHILE ASLEEP?</th>
            <th>BANGS HEAD/ROCKS BACK<br />&amp; FORTH WHILE SLEEPING?</th>
            <th>WAKES IN THE MORNING<br />FEELING TIRED?</th>
            <th>WAKES W/ A<br />DRY MOUTH?</th>
            <th>SLEEPS LATE<br />ON WKNDS?</th>
            <th>FALLS ASLEEP AT UNEXPECTED<br />TIMES DURING THE DAY</th>
            <th>FREQUENTLY BREATHES TH/<br />MOUTH WHILE AWAKE</th>
            <th>EXPERIENCES/COMPLAINS<br />ABOUT DAYTIME SLEEPINESS</th>
            <th>NORMAL GROWTH RATE HAS<br />STOPPED ON OCCASION</th>
            
            <th>HAS DIFFICULTY<br />CONCENTRATING</th>
            <th>HAS TROUBLE<br />SITTING STILL</th>
            <th>HAS BEHAVIOR<br />PROBLEMS AT SCHOOL</th>
            <th>HAS POOR PERFORMANCE<br />IN SCHOOL</th>
            <th>HAD TONSILS<br />REMOVED</th>
            <th>TOTAL<br />YES</th>
            <th>LIKELIHOOD<br />OF OSA</th>
            </tr>
            </thead>
<tbody>
	<?php foreach($recs as $record): ?>
	<?php 
		$applies_to_child = unserialize($record['applies_to_child']); 
		$applies_to_child = objectToArray($applies_to_child);
	 ?>
	<tr>
		<td><?php echo $record['last_name']; ?></td>
		<td><?php echo $record['first_name']; ?></td>
		<td><?php echo $record['child_last_name']; ?></td>
		<td><?php echo $record['child_first_name']; ?></td>
		<td><?php echo $record['patient_number']; ?></td>
		<td><?php echo $record['gender']; ?></td>
		<?php
		list($height_feet, $height_inches) = explode(":", $record['height']);
		$record['height'] = $height_feet."'";
		if((int)$height_inches > 0){
			$record['height'] .= ' '.$height_inches.'"';
		}
		?>
		<td><?php echo $record['height']; ?></td>
		<td><?php echo $record['weight']; ?></td>
		<td><?php echo $record['neck_size']; ?></td>
		<td><?php echo $record['date_of_birth']; ?></td>
		<td><?php echo $record['doc_name']; ?></td>
		<td><?php echo $record['doc_address']; ?></td>
		<td><?php echo $record['doc_city']; ?></td>
		<td><?php echo $record['doc_state']; ?></td>
		<td><?php echo $record['test_date']; ?></td>
		<td><?php echo $record['snores_everynight']; ?></td>
		<td><?php echo $record['snores_morethanhalf_night']; ?></td>
		<td><?php echo $record['snores_loudly']; ?></td>
		<td><?php echo $record['stops_breathing_while_sleeping']; ?></td>
		<td><?php echo $record['breathes_heavily_during_sleep']; ?></td>
		<td><?php echo $record['cries_while_asleep']; ?></td>
		<td><?php echo $record['rocks_head']; ?></td>
		<td><?php echo $record['wakes_up_tired']; ?></td>
		<td><?php echo $record['wakes_up_dry_mouth']; ?></td>
		<td><?php echo $record['sleeps_late_weekends']; ?></td>
		<td><?php echo $record['falls_asleep_unexpected']; ?></td>
		<td><?php echo $record['breathes_through_mouth']; ?></td>
		<td><?php echo $record['daytime_sleepiness']; ?></td>
		<td><?php echo $record['growth_rate_stop']; ?></td>
		<?php

$bool_fields = array("snores_everynight",
"snores_morethanhalf_night",
"snores_loudly",
"stops_breathing_while_sleeping",
"breathes_heavily_during_sleep",
"cries_while_asleep",
"rocks_head",
"wakes_up_tired",
"wakes_up_dry_mouth",
"sleeps_late_weekends",
"falls_asleep_unexpected",
"breathes_through_mouth",
"daytime_sleepiness",
"growth_rate_stop");


		$yes_count = 0;
		foreach($bool_fields as $fkey){
			if($record[$fkey] == "yes"){
				$yes_count++;
			}
		}
		?>
		
		<td><?php echo isset($applies_to_child['diff_conc']) ? 'yes' : 'no' ?></td>
		<td><?php echo isset($applies_to_child['trouble_ss']) ? 'yes' : 'no' ?></td>
		<td><?php echo isset($applies_to_child['probs_at_school']) ? 'yes' : 'no' ?></td>
		<td><?php echo isset($applies_to_child['poor_perf']) ? 'yes' : 'no' ?></td>
		<td><?php echo isset($applies_to_child['tonsils_taken']) ? 'yes' : 'no' ?></td>
		<td><?php echo $yes_count; ?></td>
		<td><?php echo ($yes_count >= 4) ? "POSITIVE" : "NEGATIVE" ?></td>

	</tr>
	<?php endforeach; ?>
</tbody>
</table>