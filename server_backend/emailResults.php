<?php

	$output = get_quiz_result($profile_id, $quiz_id);

	$prognosis = $output['prognosis'];	
	$response_categories = $output['response_categories'];
	$date_time = $output['date_time'];
	$profile_name = $output['profile_name'];
	$gender = $output['gender'];
	$child_conditions = $output['child_conditions'];
	$additional_info = $output['additional_info'];
	$all_responses = $output['all_responses'];
	$all_conditions = $output['all_conditions'];

	$to = $account->email;

	$subject = 'Website Change Request';

	$headers = "From: " . strip_tags('snorkid@snorkid.com') . "\r\n";
	$headers .= "Reply-To: ". strip_tags('no-reply@snorkid.com') . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message = '<html><body style="font-family: sans-serif">';
	$message .= "<table style='border-spacing: 0px; font-size: 15px;'><tbody style='text-align: center'>";
	$message .= "<tr><td style='text-align: center'><img src='http://".$_SERVER["SERVER_NAME"]."/snorkid_logo.png' /></td></tr>";
	$message .= "<tr><td style='text-align: left'>
	<p>Dear ".$output["parent_name"].",</p>

<p>Thank you for taking the SnorKid Quiz! Your child's quiz results are below. </p>

<p>To monitor your child's symptoms over time, we recommend that you take this quiz <br />

every 3 to 6 months and compare to previous results.</p>

<p>We encourage you to share these results with your child's doctor. You can forward this <br />

email to your doctor (with their permission), or you can print the report for your next <br />

office visit. </p>

<p>SnorKid is a screening quiz with proven accuracy for determining risk of sleep apnea in <br />

children. Your doctor will use this information, along with a physical exam and other <br />

health records, to help make a diagnosis. </p>

<p>Thank you again,</p>

<p>The SnorKid Team</p>
<br /><br />
</td></tr>";
	$message .= "</tbody></table>";
	$message .= "<table border='1' style='border-spacing: 0px;
width: 600px; font-size: 14px; 
border: 2px solid #006699;'><tbody>";

		$result_style = " color: #fff; background: #003366; ";
		if(strtoupper($prognosis) == 'POSITIVE'){
			$result_style = "background: green; color: white; ";
		}

	$message .= "<tr style='
		font-size: 18px;
		{$result_style} '>
		<td style='text-align: left; padding: 5px;'>RESULT:</td>
		<td style='text-align: center; width: 250px; border: 2px solid #006699;'>".strtoupper($prognosis)."</td></tr>";
	
	$message .= "<tr style='
		font-size: 12px;
		font-style: italic;
		{$result_style} '>";
	if($prognosis == "positive" ){
		$message .= "<td style='text-align: center;padding: 6px;' colspan='2'>Your child may have sleep apnea.</td></tr>";
	}
	else{
		$message .= "<td style='text-align: center;padding: 6px;' colspan='2'>Your child may not have sleep apnea.</td></tr>";
	}
	$message .= "<tr><td style='color: #006699;
font-size: 18px;
padding-left: 5px;' colspan='2'>Patient Information</td></tr>";
	$message .= "<tr><td style='padding-left: 5px;'>Parent's Name</td><td style='text-align: center'>".$output["parent_name"]."</td></tr>";	
	$message .= "<tr><td style='padding-left: 5px;'>Child's Name</td><td style='text-align: center'>{$profile_name}</td></tr>";	
	$message .= "<tr><td style='padding-left: 5px;'>Patient #</td><td style='text-align: center'>".str_pad($output["patient_number"], 6, "0", STR_PAD_LEFT)."</td></tr>";	
	$message .= "<tr><td style='padding-left: 5px;'>Doctor's Name</td><td style='text-align: center'>{$additional_info['doc_name']}</td></tr>";
	$message .= "<tr><td style='padding-left: 5px;'>Date of Test</td><td style='text-align: center'>{$date_time}</td></tr>";
	$message .= "<tr><td style='padding-left: 5px;'>Gender</td><td style='text-align: center'>{$gender}</td></tr>";
	$message .= "<tr><td style='padding-left: 5px;'>Birthdate</td><td style='text-align: center'>".$output['profile_dob']."</td></tr>";
		
	$message .= "<tr><td style='padding-left: 5px;'>Height</td><td style='text-align: center'>{$additional_info['height']}</td></tr>";
	$message .= "<tr><td style='padding-left: 5px;'>Weight</td><td style='text-align: center'>{$additional_info['weight']}</td></tr>";
	$message .= "<tr><td style='padding-left: 5px;'>Neck Size</td><td style='text-align: center'>{$additional_info['neck_size']}</td></tr>";

	$message .= "<tr><td style='color: #006699;
font-size: 18px;
padding-left: 5px;' colspan='2'>Validated Screening Questions</td></tr>";

	foreach ($all_responses as $all_resp) {
		$resp_phrase = $all_resp['response_phrase'];
		$resp_phrase = str_replace(".", "", $resp_phrase);
		$resp_phrase = str_replace("his/her", "", $resp_phrase);

		$resp_style = " background: white; color: black; ";
		if($all_resp['response'] == 'yes'){
			$resp_style = "background: green; color: white; ";
		}
		$message .= "<tr><td style='padding-left: 5px; ".$resp_style."'>".$resp_phrase."</td><td style='text-align: center; ".$resp_style."'>{$all_resp['response']}</td></tr>";
	}

	$message .= "<tr><td style='color: #006699;
font-size: 18px;
padding-left: 5px;' colspan='2'>Additional Information</td></tr>";

	foreach($all_conditions as $ckey => $cval){
		$ckey = str_replace(".", "", $ckey);
		$message .= "<tr><td style='padding-left: 5px;'>{$ckey} </td><td style='text-align: center'>{$cval}</td></tr>";
	}

	$message .= "<tr style='
		font-size: 12px;
		color: #fff;
		font-style: italic;
		background: #003366;'>";
	$message .= "<td style='text-align: center;padding: 6px;' colspan='2'>
	Four or more &quot;yes&quot; answers to the Validated Screening Questions suggest a high <br />
	likelihood of pediatric obstructive sleep apnea. Consult your physician for diagnosis and <br />
	treatment options.
	</td></tr>";
	

	$message .= "</tbody></table>";
	$message .= '</body></html>';