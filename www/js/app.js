(function(){
	'use strict';
	var base_server_url = 'http://snorkid.commongiant.com';
	angular.module('ng-scroller', []).directive('ng-iscroll', function() {
	    return {
			replace: false,
			restrict: 'A',
	        link: function(scope, element, attr){
	            scope.$watch(attr.ngScroll, function(value){
					new iScroll(document.querySelector('#wrapper'), {
		   	      	  snap: false,
		   	      	  momentum: false,
		   	      	  hScrollbar: true
		   	    });
	        });	
	       }
	    };
	});
	var app = angular.module('myApp', ['onsen.directives','ngCookies', 'ngTouch', 'ng-iscroll']);

	app.config(['$httpProvider', function($httpProvider) {
	        $httpProvider.defaults.useXDomain = true;
	        delete $httpProvider.defaults.headers.common['X-Requested-With'];
	    }
	]);

	var transformRequest = function(obj) {
						var str = [];
						for(var p in obj) {
							str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
						}
						return str.join('&');
					}

	var xhr = new SeXHR();				

	app.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
	}]);				

	app.service('QuestionService',  function() {
        var question_num = 1;
        var profile_id = 0;
        var quiz_id = 0;
        var scope = null;
        var http = null;
        var cookieStore = null;

        return {  
        	setScope: function(param_scope){
        		scope = param_scope;
        	},
        	setHttp: function(param_http){
        		http = param_http;
        	},  	
        	loadQuestion: function(profile_id, question_num){
        		this.setProfileId(profile_id);
	        	this.setQuestionNum(question_num);
	        	scope.ons.navigator.pushPage('question.html');
	        },
	        getProfileId: function () {
                return profile_id;
            },
            setProfileId: function(value) {
                profile_id = value;
            },
            getQuestionNum: function () {
                return question_num;
            },
            setQuestionNum: function(value) {
                question_num = value;
            },
            generateQuizId: function(profile_id, token, load_question_call) {            	
            	var self = this;
            	http({method: 'GET', url: base_server_url + '/generate_quiz_id/'+ profile_id + '?token='+token}).
            	success(function(data, status, headers, config) {		            					
					if(data.status == 'success'){
						self.setQuizId(data.quiz_id);
						if( typeof load_question_call !== 'undefined'){
							load_question_call.call();
						}
					}
					else if(data.error){
						alert("failed to create a quiz, please try again");
					}
				});
            },
            getQuizId: function(){
            	return quiz_id;
            },
            setQuizId: function(value){
            	quiz_id = value;
            }
        };
    })
	.controller('IndexController', ['$scope', '$http', '$window',  function($scope, $http, $window ) {
		$scope.nextSlide = function(){
			slider_handle.next();
			slider_handle.stop();
		};
		$scope.prevSlide = function(){
			slider_handle.prev();
			slider_handle.stop();
		};
	}])
	.controller('MyAccountController', ['$scope', '$http', '$window',  function($scope, $http, $window ) {

		$scope.form_message = '';
		$scope.form_message_color = 'green';

		var token = $window.localStorage.getItem('auth_token');
    	if(token){
    		$http({
				method: 'GET',
				url: base_server_url + '/account?token=' + token,
				responseType: 'json'
			}).success(function(data, status, headers, config) {
				if(data.status && data.status == "success"  ){
					var account_data = data.data;
					$scope.accountForm.first_name = account_data.first_name;
					$scope.accountForm.last_name = account_data.last_name;
					$scope.accountForm.email = account_data.email;
					
				}
				else{
					$scope.ons.navigator.pushPage('login.html');
				}					
				//
			});	
    	}
    	else{
    		$scope.ons.navigator.pushPage('login.html');
    	}

		$scope.submit = function() {	 

			var data = {};
			data.first_name = $scope.accountForm.first_name;
			data.last_name = $scope.accountForm.last_name;
			data.email = $scope.accountForm.email;

			if($scope.accountForm.password != $scope.accountForm.password_confirm){
				$scope.form_message = "password and confirmation must match.";
				$scope.form_message_color = 'red';
				return false;
			}
			else if($scope.accountForm.password){
				data.password = $scope.accountForm.password;
				data.password_confirm = $scope.accountForm.password_confirm;
			}

			$http({
				method: 'POST',
				url: base_server_url + '/update_account?token=' + token,
				dataType: 'json',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: JSON.stringify(data),
				responseType: 'json'
			}).success(function(data, status, headers, config) {					
				if(data.status == 'success'){
					$scope.form_message = "account successfully updated";
					$scope.form_message_color = 'green';
				}
				else if(data.error){
					$scope.form_message = data.error;
					$scope.form_message_color = 'red';
				}
				//
			});
		}
	}])
	.controller('LogoutController', ['$scope', '$http', '$window',  function($scope, $http, $window ) {
		$window.localStorage.removeItem('auth_token');
		$scope.ons.navigator.resetToPage('login.html');
	}])
	.controller('AccountController', ['$scope', '$http', '$window',  function($scope, $http, $window ) {

		$scope.form_message = '';
		$scope.form_message_color = 'green';
		
		$scope.submit = function() {	        

	        if($scope.accountForm.$valid){

		        var data = {};
		        data.first_name = $scope.accountForm.first_name;
		        data.last_name = $scope.accountForm.last_name;
		        data.email = $scope.accountForm.email;
		        data.password = $scope.accountForm.password;

		        //formData = $scope.form;		        
		        $http({
					method: 'POST',
					url: base_server_url + '/account',
					dataType: 'json',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: JSON.stringify(data),
					responseType: 'json'
				}).success(function(data, status, headers, config) {					
					if(data.status == 'success'){
						$scope.form_message = "account successfully created.";

						var data = {};
				        data.email = $scope.accountForm.email;
				        data.password = $scope.accountForm.password;

				        $http({
							method: 'POST',
							url: base_server_url + '/login',
							dataType: 'json',
							headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							data: JSON.stringify(data),
							responseType: 'json'
						}).success(function(data, status, headers, config) {					
							if(data.token){
								$scope.spin = false;
								$window.localStorage.setItem('auth_token',data.token);
								
								$http({method: 'GET', url: base_server_url + '/profiles?token='+data.token}).
								success(function(data, status, headers, config) {			
									if(data.length > 0){
										$scope.ons.navigator.pushPage('profile_list.html');
									}
									else{
										$scope.ons.navigator.pushPage('create_profile.html');
									}
								}).	
								error(function(data, status, headers, config) {
									$scope.ons.navigator.pushPage('create_profile.html');
								});
							}
							else if(data.error){
								$scope.spin = false;
								$scope.form_message = data.error;
								$scope.form_message_color = 'red';
							}
						});
					}
					else if(data.error){
						$scope.form_message = "failed to create an account, please try again";
						$scope.form_message_color = 'red';
					}
					//
				});
		        //

	        }
	        else{
	        	$scope.form_message = "please fill out all the fields and try again";
	        	$scope.form_message_color = 'red';
	        }

	        
	    };
	    $scope.loadLogin = function() {
	    	var token = $window.localStorage.getItem('auth_token');
	    	if(token){
	    		$http({
					method: 'GET',
					url: base_server_url + '/token_verify/' + token,
					responseType: 'json'
				}).success(function(data, status, headers, config) {
					if(data.status && data.status == "valid_token"  ){

						$http({method: 'GET', url: base_server_url + '/profiles?token='+token}).
						success(function(data, status, headers, config) {			
							if(data.length > 0){
								$scope.ons.navigator.pushPage('profile_list.html');
							}
							else{
								$scope.ons.navigator.pushPage('create_profile.html');
							}
						}).	
						error(function(data, status, headers, config) {
							$scope.ons.navigator.pushPage('create_profile.html');
						});
						
					}
					else{
						$scope.ons.navigator.pushPage('login.html');
					}					
					//
				});	
	    	}
	    	else{
	    		$scope.ons.navigator.pushPage('login.html');
	    	}
	        
	    };
	}])
	.controller('EmailResultsController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		var profile_id = QuestionService.getProfileId();
		var quiz_id = QuestionService.getQuizId();

		$http({method: 'GET', url: base_server_url + '/email_results/'+ profile_id + '/' + quiz_id +  '?token='+token}).
		success(function(data, status, headers, config) {
			$scope.prognosis = data.prognosis;	
			$scope.response_categories = data.response_categories;
			$scope.date_time = data.date_time;
			$scope.profile_name = data.profile_name;
		}).	
		error(function(data, status, headers, config) {
			//
		});

	}])
	.controller('ResultsController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		var profile_id = QuestionService.getProfileId();
		var quiz_id = QuestionService.getQuizId();

		$http({method: 'GET', url: base_server_url + '/results/'+ profile_id + '/' + quiz_id +  '?token='+token}).
		success(function(data, status, headers, config) {
			$scope.prognosis = data.prognosis;	
			$scope.response_categories = data.response_categories;
			$scope.date_time = data.date_time;
			$scope.profile_name = data.profile_name;
			$scope.gender = data.gender;
			$scope.child_conditions = data.child_conditions;
		}).	
		error(function(data, status, headers, config) {
			//
		});

	}])
	.controller('QuizListController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		var profile_id = QuestionService.getProfileId();

		$scope.profile_id = profile_id;
		$http({method: 'GET', url: base_server_url + '/list_quizzes/'+ profile_id + '?token='+token}).
		success(function(data, status, headers, config) {	
			$scope.quizzes = data;
		}).	
		error(function(data, status, headers, config) {
			//
		});

		$scope.loadQuestion = function(profile_id, quiz_id, question_num){
			QuestionService.setScope($scope);
			QuestionService.setQuizId(quiz_id);
			QuestionService.loadQuestion(profile_id, question_num);
		}

	}])
	.controller('QuestionController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		var profile_id = QuestionService.getProfileId();
		var quiz_id = QuestionService.getQuizId();
		var question_num = QuestionService.getQuestionNum();

		$scope.profile_id = profile_id;
		$scope.question_id = question_num;
		$scope.category_image = "";
		$scope.next_sequence = parseInt(question_num) + 1;
		$scope.question_text = "loading..";
		$scope.response = '';

		$http({method: 'GET', url: base_server_url + '/question/'+ profile_id + '/' + quiz_id + '/'+  question_num + '?token='+token}).
		success(function(data, status, headers, config) {			
			$scope.question_text = data.question_text;
			$scope.sequence = data.sequence;
			$scope.question_id = data.id;
			$scope.category_name = data.category_name;
			$scope.category_image = "img/" + data.category_image;
			$scope.response = data.response;
			var percentage  = data.sequence / 14;
			percentage = percentage * 100.0;
			$scope.percentage = percentage;

			$scope.unanswered = false;
			if(data.response != 'yes' && data.response != 'no'){
				$scope.unanswered = true;
			}

		}).	
		error(function(data, status, headers, config) {
			//
		});

		$scope.loadQuestion = function(profile_id, question_num){
			if(question_num > 14){
				$scope.ons.navigator.pushPage('additional_info_1.html');
			}
			else{
				QuestionService.setScope($scope);
				QuestionService.loadQuestion(profile_id, question_num);
			}
			
		}

		$scope.answerQuestion = function(answer, question_id){
			var profile_id = QuestionService.getProfileId();
			var quiz_id = QuestionService.getQuizId();
			var data = {};
			data.quiz_id = quiz_id;
			data.profile_id = profile_id;
			data.question_id = question_id;
			data.response = answer;
			$scope.response = answer;

			QuestionService.setScope($scope);
			$http({
				method: 'POST', 
				url: base_server_url + '/question_response' + '?token='+token,
				dataType: 'json',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: JSON.stringify(data),
				responseType: 'json'

			}).
			success(function(data, status, headers, config) {	
				if(data.status == "success"){
					$scope.unanswered = false;
					if(data.next_sequence > 0){
						QuestionService.loadQuestion(profile_id, data.next_sequence);	
					}
					else{
						$scope.ons.navigator.pushPage('additional_info_1.html');
					}					
				}	
			}).	
			error(function(data, status, headers, config) {
				//
			});

		}

	}])
	.controller('AdditionalInfoController', ['$scope', '$http', '$window', '$timeout', 'QuestionService',  function($scope, $http, $window, $timeout, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		var profile_id = QuestionService.getProfileId();
		var quiz_id = QuestionService.getQuizId();
		$scope.sectionvalue = "sect";

		$scope.init1 = function () {
			$http({
				method: 'GET', 
				url: base_server_url + '/additional_info/' + profile_id + '/' + quiz_id + '/' + 'section1' + '?token='+token,			
			}).
			success(function(data, status, headers, config) {	
				$scope.additionalInfoForm.height = data.height;
				$scope.additionalInfoForm.weight = data.weight;
				$scope.additionalInfoForm.neck_size = data.neck_size;	
			}).	
			error(function(data, status, headers, config) {
				//
			});
		}

		$scope.submitForm1 = function() {

			var data = {};
			if($scope.additionalInfoForm.$valid){
				$scope.form_message = '';
				data.gender = $scope.additionalInfoForm.gender;
				data.height = $scope.additionalInfoForm.height.feet + ":" + $scope.additionalInfoForm.height.inches;
				data.weight = $scope.additionalInfoForm.weight;
				data.neck_size = $scope.additionalInfoForm.neck_size;
				data.section = "section1";
				data.profile_id = profile_id;
				data.quiz_id = quiz_id;
				processor(data);
			}
			else{
	        	$scope.form_message = 'please fill out all the fields and try again';
				$scope.form_message_color = 'red';
	        }

		}

		$scope.init2 = function () {			
			$http({
				method: 'GET', 
				url: base_server_url + '/additional_info/' + profile_id + '/' + quiz_id + '/' + 'section2' + '?token='+token,			
			}).
			success(function(data, status, headers, config) {					
				for (var key in data) {
					$scope[key] = data[key];
				}
			}).	
			error(function(data, status, headers, config) {
				//
			});
		}

		$scope.submitForm2 = function() {

			var data = {};
			if($scope.additionalInfoForm){
				//console.log($scope.additionalInfoForm.applies_to_child);	
				data.applies_to_child = {};
				data.applies_to_child.diff_conc = $scope.diff_conc;
				data.applies_to_child.trouble_ss = $scope.trouble_ss;
				data.applies_to_child.probs_at_school = $scope.probs_at_school;
				data.applies_to_child.poor_perf = $scope.poor_perf;
				data.applies_to_child.tonsils_taken = $scope.tonsils_taken;

				data.section = "section2";
				data.profile_id = profile_id;
				data.quiz_id = quiz_id;
				processor(data);
			}

		}

		$scope.init3 = function () {
			$http({
				method: 'GET', 
				url: base_server_url + '/additional_info/' + profile_id + '/' + quiz_id + '/' + 'section3' + '?token='+token,			
			}).
			success(function(data, status, headers, config) {	
				$scope.additionalInfoForm.doc_name = data.doc_name;
				$scope.additionalInfoForm.doc_address = data.doc_address;
				$scope.additionalInfoForm.doc_city = data.doc_city;
				$scope.additionalInfoForm.doc_state = data.doc_state;	
			}).	
			error(function(data, status, headers, config) {
				//
			});
		}

		$scope.submitForm3 = function() {

			var data = {};
			if($scope.additionalInfoForm.$valid){	
				data.doc_name = $scope.additionalInfoForm.doc_name;
				data.doc_address = $scope.additionalInfoForm.doc_address;
				data.doc_city = $scope.additionalInfoForm.doc_city;
				data.doc_state = $scope.additionalInfoForm.doc_state;
				data.section = "section3";
				data.profile_id = profile_id;
				data.quiz_id = quiz_id;
				processor(data);
			}
			else{
	        	$scope.form_message = 'please fill out all the fields and try again';
				$scope.form_message_color = 'red';
	        }

		}

		function processor(data){
			$http({
				method: 'POST', 
				url: base_server_url + '/additional_info' + '?token='+token,
				dataType: 'json',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: JSON.stringify(data),
				responseType: 'json'

			}).
			success(function(data, status, headers, config) {	
				if(data.status == "success"){
					$scope.ons.navigator.pushPage(data.redirect);				
				}	
			}).	
			error(function(data, status, headers, config) {
				//
			});
		}


	}])
	.controller('PrognosisController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		var profile_id = QuestionService.getProfileId();
		var quiz_id = QuestionService.getQuizId();
		
		var negativeProgSwiper = $('.banner.negative.prognosis').swiper({
			//Your options here:
			slideElement: 'li',
			mode:'horizontal',
			loop: false,
			pagination: '.negative.prognosis .pagination',
			paginationElement: 'div',
			paginationClickable: true,
			createPagination : true
			//etc..
		});

		var positiveProgSwiper = $('.banner.positive.prognosis').swiper({
			//Your options here:
			slideElement: 'li',
			mode:'horizontal',
			loop: false,
			pagination: '.positive.prognosis .pagination',
			paginationElement: 'div',
			paginationClickable: true,
			createPagination : true
			//etc..
		});
	  
		
		$http({method: 'GET', url: base_server_url + '/prognosis/' + profile_id + '/' + quiz_id + '?token='+token }).
		success(function(data, status, headers, config) {			
			$scope.prognosis = data.prognosis;
		}).	
		error(function(data, status, headers, config) {
			//
		});

	}])
	.controller('ProfileListController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		var token = $window.localStorage.getItem('auth_token');
		$scope.scroller_height = '460';

		$http({method: 'GET', url: base_server_url + '/profiles?token='+token}).
		success(function(data, status, headers, config) {			
			$scope.profiles = data;
			var height = (data.length * 101) + 80;
			$scope.scroller_height = height + '';
		}).	
		error(function(data, status, headers, config) {
			//
		});

		$scope.loadNewQuiz = function(profile_id){
			QuestionService.setScope($scope);
			QuestionService.setHttp($http);
			QuestionService.generateQuizId(profile_id, token, function(){
				QuestionService.loadQuestion(profile_id, 1);	
			});
			
		}

		$scope.loadQuizzes = function(profile_id){
			QuestionService.setScope($scope);
			QuestionService.setProfileId(profile_id);
			$scope.ons.navigator.pushPage('previous_quizzes.html');
		}

	}])
	.controller('ProfileController', ['$scope', '$http', '$window', 'QuestionService',  function($scope, $http, $window, QuestionService ) {
		
		$scope.form_message = '';
		$scope.form_message_color = 'green';

		$scope.submit = function() {	        

	        if($scope.profileForm.$valid){

		        var data = {};
		        data.child_name = $scope.profileForm.child_name;
		        data.child_last_name = $scope.profileForm.child_last_name;
		        data.dob = $scope.profileForm.dob;
		        data.gender = $scope.profileForm.gender;

		        var token = $window.localStorage.getItem('auth_token');

		        //formData = $scope.form;		        
		        $http({
					method: 'POST',
					url: base_server_url + '/profile?token='+token,
					dataType: 'json',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: JSON.stringify(data),
					responseType: 'json'
				}).success(function(data, status, headers, config) {					
					if(data.status == 'success'){						
						QuestionService.setScope($scope);
						QuestionService.setHttp($http);
						QuestionService.setProfileId(data.profile_id);
						QuestionService.generateQuizId(data.profile_id, token, function(){
							QuestionService.loadQuestion(data.profile_id, 1);	
						});
						
					}
					else if(data.error){
						$scope.form_message = 'failed to create a profile, please try again';
						$scope.form_message_color = 'red';
					}
					//
				});
		        //

	        }
	        else{
	        	$scope.form_message = 'please fill out all the fields and try again';
				$scope.form_message_color = 'red';
	        }

	        
	    };
	    $scope.mySecondFunction = function(msg) {
	         alert(msg + '!!! second function call!');   
	    };
	}])
	.controller('ResetController', ['$scope', '$http', '$window', '$timeout', function($scope, $http, $window, $timeout) {
		$scope.form_message = '';
		$scope.form_message_color = 'green';

		$scope.submit = function() {
			if($scope.resetForm.$valid){
				
				var data = {};
		        data.username = $scope.resetForm.username;

				$http({
					method: 'POST',
					url: base_server_url + '/send_reset_link',
					dataType: 'json',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: JSON.stringify(data),
					responseType: 'json'
				}).success(function(data, status, headers, config) {					
					if(data.status == 'success'){
						$scope.form_message = "Request Sent: please check your email to complete your password reset.";
					}
					else if(data.error){
						$scope.form_message = "failed to reset your password, please try again";
						$scope.form_message_color = 'red';
					}
				});
			}
			else{
				$scope.form_message = "invalid input. please try again.";
				$scope.form_message_color = 'red';
			}
		}
	}])
	.controller('LoginController', ['$scope', '$http', '$window', '$timeout', function($scope, $http, $window, $timeout) {
		var token = $window.localStorage.getItem('auth_token');

		$scope.form_message = '';
		$scope.form_message_color = 'green';
		$scope.spin = false;

		$scope.resetPassword = function() {
			$scope.ons.navigator.pushPage('reset_password.html');
		}
		
		$scope.submit = function() {

			if($scope.loginForm.$valid){

				$scope.form_message = 'please wait..';
				$scope.form_message_color = 'black';
				$scope.spin = true;
				
				var data = {};
		        data.email = $scope.loginForm.username;
		        data.password = $scope.loginForm.password;

		        $http({
					method: 'POST',
					url: base_server_url + '/login',
					dataType: 'json',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data: JSON.stringify(data),
					responseType: 'json'
				}).success(function(data, status, headers, config) {					
					if(data.token){
						$scope.spin = false;
						$window.localStorage.setItem('auth_token',data.token);
						
						$http({method: 'GET', url: base_server_url + '/profiles?token='+data.token}).
						success(function(data, status, headers, config) {			
							if(data.length > 0){
								$scope.ons.navigator.pushPage('profile_list.html');
							}
							else{
								$scope.ons.navigator.pushPage('create_profile.html');
							}
						}).	
						error(function(data, status, headers, config) {
							$scope.ons.navigator.pushPage('create_profile.html');
						});
					}
					else if(data.error){
						$scope.spin = false;
						$scope.form_message = data.error;
						$scope.form_message_color = 'red';
					}
				});



			} else{
				$scope.form_message = 'invalid input. please try again.';
				$scope.form_message_color = 'red';
			}
		}

	}]);

})();
